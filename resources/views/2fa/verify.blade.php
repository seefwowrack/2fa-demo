<form action="" method="get">
    Email <br>
    <input type="text" name="email" value="{{(isset($_GET['email'])) ? $_GET['email'] : ''}}"/> <br>
    Code <br>
    <input type="text" name="code" value="{{(isset($_GET['code'])) ? $_GET['code'] : ''}}"/> <br>
    <input type="submit" value="submit"><br>
</form>

@if(isset($_GET['email']) && isset($_GET['code']))
@php

    $user = DB::table('two_factor_auths')->where('email',$_GET['email'])->first();
    if($user){
        $tfa = new RobThree\Auth\TwoFactorAuth('Wowrack');
        $verify = $tfa->verifyCode($user->secret, $_GET['code'],0);
        if($verify){
            echo '<span style="color:#0c0">VALID!</span>';
        }else{
            echo '<span style="color:#c00">INVALID!</span>';
        }
    }else{
        echo "User Email Does Not Exist, please generate new on /generate";
    }

    
@endphp

@endif

<ul>
    <li><a href="/"><< Back to Main</a></li>
</ul>