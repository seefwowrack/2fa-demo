<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TwoFactorController; 
// use RobThree;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // $tfa = new RobThree\Auth\TwoFactorAuth('Wowrack');
//     // dd($tfa);
//     $tfa = new RobThree\Auth\TwoFactorAuth('Wowrack');
//     $secret = $tfa->createSecret(160);
//     $qr_code = $tfa->getQRCodeImageAsDataUri('My label', $secret);
//     $cunk_split= chunk_split($secret, 4, ' ');
//     $code = $tfa->getCode($secret);
//     $verify_code =$tfa->verifyCode($secret, $code);
//     // dd([$tfa,$secret,$qr_code,$cunk_split,$code,$verify_code]);
//     return view('test');
// });


Route::get('/',[TwoFactorController::class, 'index']);
Route::get('/generate',[TwoFactorController::class, 'generate']);
Route::get('/verify',[TwoFactorController::class, 'verify']);