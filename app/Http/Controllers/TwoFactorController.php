<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


class TwoFactorController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
    public function index(){
        return view('2fa.main');
    }


    public function generate(){
        return view('2fa.generate');
    }
    
    public function verify(){
        return view('2fa.verify');
    }
}